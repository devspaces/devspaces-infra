resource "random_string" "storage-bucket-suffix" {
  length = 16
  special = false
  upper = false
}

resource "google_storage_bucket" "encrypted-keys" {
  name          = "mdp_encrypted_keys_${random_string.storage-bucket-suffix.result}"
  location      = var.location 
  force_destroy = true

  bucket_policy_only = true
}

resource "google_storage_bucket" "build-artifacts" {
  name          = "devspaces_build_artifacts_${random_string.storage-bucket-suffix.result}"
  location      = var.location
  force_destroy = true

  bucket_policy_only = true
}

resource "google_storage_bucket_object" "encrypted-key" {
  name   = "ssh-key.enc"
  source = "ssh-key.enc"
  bucket = google_storage_bucket.encrypted-keys.name
}

resource "google_storage_bucket_object" "public-key" {
  name   = "ssh-key.pub"
  source = "ssh-key.pub"
  bucket = google_storage_bucket.encrypted-keys.name
}
