variable project {
  type = string
  default = ""
  description = "Project id where resources will be provisioned."
}

variable location {
  type = string
  default = ""
  description = "Location where resources will be created."
}

