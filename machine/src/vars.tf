variable project {
  type        = string
  default     = ""
  description = "Project id where resources will be provisioned."
}

variable location {
  type        = string
  description = "Zone in which to provision cloud environment."
}

variable dev_machine_name {
  type        = string
  description = "Name of the development machine."
}

variable use_persistent_storage {
  type        = bool
  default     = false
  description = "Set to true if you wish to create a persistent disk."
}

variable vm_exists {
  type        = bool
  default     = true
  description = "Set to false if you wish to remove a VM but keep persistent disk."
}
