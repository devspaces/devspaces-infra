resource "google_compute_instance" "dev-machine" {
  count        = var.vm_exists ? 1 : 0
  name         = var.dev_machine_name
  machine_type = "n1-standard-1"
  zone         = var.location
  min_cpu_platform = "Intel Haswell"

  boot_disk {
    initialize_params {
      image = "ubuntu-2004-nested-vmx"
    }
  }

  dynamic "attached_disk" {
    for_each = google_compute_disk.workspace-disk.*.id
    iterator = disk
    content {
      source = disk.value
      device_name = "workspace"
    }
  }

  network_interface {
    network = "default"

    access_config {
      // Ephemeral IP
    }
  }

  metadata = {
    ssh-keys = join(" ", [ "mvuksano:", file("ssh-key.pub") ])
  }

  metadata_startup_script = templatefile("${path.module}/init.tmpl", { username  = "mvuksano",
                                                                        gs_bucket = "mdp_workspace_storage_jqhr0vr3745oxnou",
                                                                        repos    = [
                                                                          { url = "git@gitlab.com:mvuksano/cpp-starter-kit.git", path = "~/devel/cpp-starter-kit" },
                                                                          { url = "git@gitlab.com:mvuksano/kvm-playground.git", path = "~/devel/kvm-playground" },
                                                                          { url = "git@github.com:markovuksanovic/io_uring_examples.git", path = "~/devel/io_uring_examples" }
                                                                        ]
                                                                      })

  service_account {
    scopes = ["cloud-platform"]
  }
}

resource "google_compute_disk" "workspace-disk" {
  count = var.use_persistent_storage ? 1 : 0
  name  = "${var.dev_machine_name}-workspace-disk"
  type  = "pd-ssd"
  zone  =  var.location
  size  = 10
}

output "instance_ip_addr" {
  value = google_compute_instance.dev-machine.*.network_interface.0.access_config.0.nat_ip
}
