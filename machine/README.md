# Find all processes that are accessing a file system:

1. sudo lsof +f -- /home/mvuksano/workspace
1. sudo fuser /home/mvuksano/workspace

Both `fuser` and `lsof` allow killing processes that are using files

# Upload composite file using gsutil
1. gsutil -o GSUtil:parallel_composite_upload_threshold=1.5G cp /mnt/disks/nvme0n1/workspace.disk gs://mdp_workspace_storage_jqhr0vr3745oxnou/workspace.disk

# Test performance when accessing a GCS bucket:
1. gsutil perfdiag -t wthru_file -s 100M gs://mdp_workspace_storage_jqhr0vr3745oxnou/
