terraform {
  required_providers {
    google = "~> 3.16"
  }
}

provider "google" {
  credentials = file("account.json")
  project     = var.project 
}

resource "google_project_service" "service" {
  for_each = toset(["iam.googleapis.com", "cloudkms.googleapis.com", "sourcerepo.googleapis.com"])
  service = each.key 

  project = var.project 
  disable_on_destroy = false
}


