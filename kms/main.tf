resource "random_string" "kms-suffix" {
  length = 16
  special = false
}

resource "google_kms_key_ring" "ssh-keys-keyring" {
  name     = "ssh-keys-${random_string.kms-suffix.result}"
  location = "global"
}

resource "google_kms_crypto_key" "ssh-enc-dec-key" {
  name     = "ssh-enc-dec-key"
  key_ring = google_kms_key_ring.ssh-keys-keyring.self_link
  purpose  = "ENCRYPT_DECRYPT"

  version_template {
    algorithm = "GOOGLE_SYMMETRIC_ENCRYPTION"
  }

  lifecycle {
    prevent_destroy = true
  }
}

resource "google_kms_crypto_key_iam_member" "crypto_key" {
  crypto_key_id = google_kms_crypto_key.ssh-enc-dec-key.id
  role          = "roles/cloudkms.cryptoKeyDecrypter"
  member        = "serviceAccount:870266752251-compute@developer.gserviceaccount.com"
}

