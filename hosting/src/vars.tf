variable project {
  type        = string
  default     = ""
  description = ""
}

variable location {
  type        = string
  default     = "us-west2-b"
  description = ""
}
