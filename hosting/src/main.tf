resource "google_cloud_run_domain_mapping" "staging-devspaces-ui" {
  location = var.location 
  name     = "staging.devspaces.dev"

  metadata {
    namespace = var.project
  }

  spec {
    route_name = "devspaces-ui"
  }
}

resource "google_cloud_run_domain_mapping" "staging-devspaces-services" {
  location = var.location 
  name     = "services-staging.devspaces.dev"

  metadata {
    namespace = var.project
  }

  spec {
    route_name = "devspaces-provisioner" 
  }
}

resource "random_id" "db-suffix" {
  byte_length = 8
}

resource "random_password" "db-password" {
  length = 16
  special = true
  override_special = "_%@"
}

resource "google_sql_database_instance" "devspaces-db-instance" {
  name             = "devspaces-db-${random_id.db-suffix.dec}"
  database_version = "POSTGRES_12"
  region           = var.location

  settings {
    tier = "db-f1-micro"
  }
}

resource "google_sql_user" "users" {
  name     = "devspaces-provisioner"
  instance = google_sql_database_instance.devspaces-db-instance.name
  password = random_password.db-password.result
}

resource "google_sql_database" "database" {
  name     = "devspaces-staging"
  instance = google_sql_database_instance.devspaces-db-instance.name
}

resource "google_secret_manager_secret" "db-password-secret" {
  secret_id = "${google_sql_database_instance.devspaces-db-instance.name}-password"

  replication {
    automatic = true
  }
}


resource "google_secret_manager_secret_version" "db-password-secret-version" {
  secret = google_secret_manager_secret.db-password-secret.id

  secret_data = random_password.db-password.result

}
