terraform {
  required_providers {
    google = "~> 3.16"
  }
  backend "gcs" {
    bucket  = "tf-state-marko-dev-platform"
  }
}

provider "google" {
  project     = var.project 
}
