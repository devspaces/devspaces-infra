terraform {
  required_providers {
    google = "~> 3.16"
  }
  backend "gcs" {
    bucket  = "tf-state-marko-dev-platform"
    prefix  = "repo/"
  }
}

provider "google" {
  credentials = file("account.json")
  project     = var.project
}
