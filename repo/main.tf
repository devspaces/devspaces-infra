resource "google_sourcerepo_repository" "repo" {
  name = "marko-dev-platform/code"
}

# Source code repo for facebook practice
resource "google_sourcerepo_repository" "fb-practice-repo" {
  name = "marko-dev-platform/fb-practice"
}

# Playground for learning boost
resource "google_sourcerepo_repository" "boost-playground" {
  name = "marko-dev-platform/boost-playground"
}

# Simple implementation of webserver using boost
resource "google_sourcerepo_repository" "boost-ws" {
  name = "marko-dev-platform/boost-ws"
}
